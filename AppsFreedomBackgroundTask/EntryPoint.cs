﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Background;

namespace AppsFreedomBackgroundTask
{
    public sealed class EntryPoint : IBackgroundTask
    {
        public async void Run(IBackgroundTaskInstance taskInstance)
        {
            var defferal = taskInstance.GetDeferral();

            string text = String.Format("Background: {0}", DateTime.Now.ToString());
            var writer = new DatabaseWriter.Writer();
            await writer.WriteAsync(text);

            defferal.Complete();
        }
    }
}