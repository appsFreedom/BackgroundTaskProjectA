﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace DatabaseWriter
{
    public class Writer
    {
        public async Task WriteAsync(string text)
        {
            StorageFile file = await KnownFolders.VideosLibrary.CreateFileAsync("appsFreedom.txt", CreationCollisionOption.OpenIfExists);
            await FileIO.AppendLinesAsync(file, new string[] { text });
        }
    }
}
