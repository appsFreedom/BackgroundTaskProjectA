﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading;
using System.Threading.Tasks;
using Windows.Data.Xml.Dom;
using Windows.ApplicationModel.ExtendedExecution;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Notifications;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.System;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace BackgroundTaskProjectA
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            await WriteDateAsync();
            BackgroundTaskHelper.StartBackgroundTask();
        }

        private async void StartBackgroundTask_Click(object sender, RoutedEventArgs e)
        {
            await WriteDateAsync();
            BackgroundTaskHelper.StartBackgroundTask();
        }

        private void StopBackgroundTask_Click(object sender, RoutedEventArgs e)
        {
            BackgroundTaskHelper.StopBackgroundTask();
        }

        private async Task WriteDateAsync()
        {
            string text = String.Format("Foreground: {0}", DateTime.Now.ToString());
            var writer = new DatabaseWriter.Writer();
            await writer.WriteAsync(text);
        }
    }
}