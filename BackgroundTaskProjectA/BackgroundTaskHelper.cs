﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Background;

namespace BackgroundTaskProjectA
{
    public static class BackgroundTaskHelper
    {
        public static async void StartBackgroundTask()
        {
            var backgroundAccessStatus = await BackgroundExecutionManager.RequestAccessAsync();
            if (backgroundAccessStatus == BackgroundAccessStatus.AlwaysAllowed ||
                backgroundAccessStatus == BackgroundAccessStatus.AllowedSubjectToSystemPolicy)
            {
                if (!IsTaskAlreadyRegister())
                {
                    BackgroundTaskBuilder taskBuilder = new BackgroundTaskBuilder();
                    taskBuilder.Name = TASK_NAME;
                    taskBuilder.TaskEntryPoint = ENTRY_POINT;
                    taskBuilder.SetTrigger(new TimeTrigger(15, false));
                    var task = taskBuilder.Register();
                }
            }
        }

        public static void StopBackgroundTask()
        {
            foreach (var task in BackgroundTaskRegistration.AllTasks)
            {
                if (task.Value.Name == TASK_NAME)
                {
                    task.Value.Unregister(true);
                }
            }
        }

        private static bool IsTaskAlreadyRegister()
        {
            foreach (var task in BackgroundTaskRegistration.AllTasks)
            {
                if (task.Value.Name == TASK_NAME)
                {
                    return true;
                }
            }

            return false;
        }

        private const string TASK_NAME = "AppsFreedomBackgroundTask";
        private const string ENTRY_POINT = "AppsFreedomBackgroundTask.EntryPoint";
    }
}
